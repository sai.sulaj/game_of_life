mod gol;
use gol::gol::GameOfLife;
use std::{thread, time, env};

fn collect_args() -> Vec<u32> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 4 {
        panic!("Requires 2 arguments: dimension, seed number, update period in ms");
    }
    args[1..].to_owned().iter().map(|x| x.parse().ok().unwrap()).collect()
}

fn main() {
    let args = collect_args();
    let period = time::Duration::from_millis(args[2] as u64);
    let mut gol = GameOfLife::new(args[0] as usize);
    gol.seed(args[1]);
    println!("{}", gol);
    let mut prev_living = gol.sum_living();
    loop {
        thread::sleep(period);
        gol.iterate();
        println!("{}", gol);
        let new_total = gol.sum_living();
        let is_delta_neg = new_total < prev_living;
        let delta = if is_delta_neg { prev_living - new_total } else { new_total - prev_living };
        let sign = if is_delta_neg { "-" } else { "" };
        println!("Total living: {}, delta: {}{}, iteration: {}", new_total, sign, delta, gol.iteration);
        prev_living = new_total;
    }
}

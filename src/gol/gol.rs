use std::{
    fmt,
    cmp,
};
use rand;
use rand::Rng;

pub struct GameOfLife {
    pub iteration: u64,
    dim: usize,
    state: Vec<Vec<u8>>,
}
impl GameOfLife {
    pub fn new(dim: usize) -> Self {
        GameOfLife {
            iteration: 0,
            state: vec![vec![0; dim]; dim],
            dim,
        }
    }
    pub fn seed(&mut self, k: u32) {
        let mut rng = rand::thread_rng();
        for _ in 0..k {
            let mut seed: f32 = rng.gen();
            let x: usize = (seed * self.dim as f32).floor() as usize;
            seed = rng.gen();
            let y: usize = (seed * self.dim as f32).floor() as usize;

            self.state[y][x] = 1;
        }
    }
    pub fn iterate(&mut self) {
        self.iteration += 1;
        for y in 0..self.dim {
            for x in 0..self.dim {
                self.update_cell(x, y);
            }
        }
    }
    fn update_cell(&mut self, x: usize, y: usize) {
        let sum = self.sum_neighbors(x, y, 1);

        if self.state[y][x] == 1 {
            if sum < 2 {
                self.state[y][x] = 0;
            } else if sum > 3 {
                self.state[y][x] = 0;
            }
        } else {
            if sum == 3 {
                self.state[y][x] = 1;
            }
        }
    }
    fn sum_neighbors(&self, x: usize, y: usize, range: usize) -> u8 {
        let mut sum = 0;

        let y_min = if y < range { 0 } else { y - range };
        for yi in y_min..=cmp::min(self.dim - range, y + range) {
            let x_min = if x < range { 0 } else { x - range };
            for xi in x_min..=cmp::min(self.dim - range, x + range) {
                if xi != x || yi != y {
                    sum += self.state[yi][xi];
                }
            }
        }

        sum
    }
    pub fn sum_living(&self) -> u32 {
        self.state.iter().fold(0, |acc, y| {
            acc + y.iter().sum::<u8>() as u32
        })
    }
}

impl fmt::Display for GameOfLife {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for _ in 0..50 { write!(f, "\n"); }

        for _ in 0..(self.dim * 2 + 2) { write!(f, "0"); }
        write!(f, "\n");

        for y in &self.state {
            write!(f, "0");
            for x in y {
                if *x == 1 {
                    write!(f, "██");
                } else {
                    write!(f, "  ");
                }
            }
            write!(f, "0\n");
        }

        for _ in 0..(self.dim * 2 + 2) { write!(f, "0"); }
        write!(f, "")
    }
}
